package com.vesodev.dndtools.model;

/**
 * Created by vesko on 22.5.2021 г..
 */
public enum DiceType {
    D4, D6, D8, D10, D12, D20, D100
}

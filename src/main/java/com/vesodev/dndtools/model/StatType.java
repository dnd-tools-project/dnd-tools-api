package com.vesodev.dndtools.model;

/**
 * Created by vesko on 22.5.2021 г..
 */
public enum StatType {
    Strength, Dexterity, Constitution, Intelligence, Wisdom, Charisma
}

package com.vesodev.dndtools.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by vesko on 25.6.2021 г..
 */
//@Entity
public class Race {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @Column(length = 3000)
    private String description;

    private Boolean isSubRace;

    @ManyToMany
    private List<Race> subRaces;

    @ManyToMany
    private List<StatBonus> statBonuses;

    @ManyToMany
    private List<Skill> skillProficiencies;

    @ManyToMany
    private List<Language> languages;

    @ManyToMany
    private List<Trait> traits;

    private Integer speed;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getSubRace() {
        return isSubRace;
    }

    public void setSubRace(Boolean subRace) {
        isSubRace = subRace;
    }

    public List<Race> getSubRaces() {
        return subRaces;
    }

    public void setSubRaces(List<Race> subRaces) {
        this.subRaces = subRaces;
    }

    public List<StatBonus> getStatBonuses() {
        return statBonuses;
    }

    public void setStatBonuses(List<StatBonus> statBonuses) {
        this.statBonuses = statBonuses;
    }

    public List<Skill> getSkillProficiencies() {
        return skillProficiencies;
    }

    public void setSkillProficiencies(List<Skill> skillProficiencies) {
        this.skillProficiencies = skillProficiencies;
    }

    public List<Language> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    public List<Trait> getTraits() {
        return traits;
    }

    public void setTraits(List<Trait> traits) {
        this.traits = traits;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}

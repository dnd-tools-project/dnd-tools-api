package com.vesodev.dndtools.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by vesko on 24.5.2021 г..
 */
@Entity
public class SavingThrow {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private StatType type;

    private boolean hasProficiency = false;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public StatType getType() {
        return type;
    }

    public void setType(StatType type) {
        this.type = type;
    }

    public Boolean getHasProficiency() {
        return hasProficiency;
    }

    public void setHasProficiency(Boolean hasProficiency) {
        this.hasProficiency = hasProficiency;
    }
}

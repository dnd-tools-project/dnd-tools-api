package com.vesodev.dndtools.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by vesko on 22.5.2021 г..
 */
@Entity
public class Character {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(unique = true)
    private String name;

    private String playerName;

    private Integer maxHp;

    private Integer currentHp;

    private Integer armorClass;

    private Integer speed;

    @OneToOne()
    private Dice hitDice;

    private String charClass;

    private String race;

    private Integer experiencePoints;

    @ManyToMany()
    private List<Skill> skills;

    @ManyToMany()
    private List<Stat> stats;

    @ManyToMany()
    private List<Spell> spells;

    private Integer gold;

    private Integer silver;

    @ManyToMany()
    private List<SavingThrow> savingThrows;

    private Integer proficiencyBonus;

    private Alignment alignment;

    @ManyToMany()
    private List<Item> items;

    @Column(length = 100000)
    private String notes;

    @ManyToMany()
    private List<SpellSlot> spellSlots;

    private Integer level;

    @Column(nullable = true)
    private Boolean inspired = false;

    @OneToOne
    private Weapon currentWeapon;

    @Column(length = 100000)
    private String proficiencies;

    @ManyToMany()
    private List<Counter> counters;

    @ManyToMany()
    private List<Proficiency> proficienciesObjs;

    @ManyToMany()
    private List<Trait> traits;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getMaxHp() {
        return maxHp;
    }

    public void setMaxHp(Integer maxHp) {
        this.maxHp = maxHp;
    }

    public Integer getCurrentHp() {
        return currentHp;
    }

    public void setCurrentHp(Integer currentHp) {
        this.currentHp = currentHp;
    }

    public Integer getArmorClass() {
        return armorClass;
    }

    public void setArmorClass(Integer armorClass) {
        this.armorClass = armorClass;
    }

    public Integer getSpeed() {
        return speed;
    }

    public void setSpeed(Integer speed) {
        this.speed = speed;
    }

    public Dice getHitDice() {
        return hitDice;
    }

    public void setHitDice(Dice hitDice) {
        this.hitDice = hitDice;
    }

    public String getCharClass() {
        return charClass;
    }

    public void setCharClass(String charClass) {
        this.charClass = charClass;
    }

    public String getRace() {
        return race;
    }

    public void setRace(String race) {
        this.race = race;
    }

    public Integer getExperiencePoints() {
        return experiencePoints;
    }

    public void setExperiencePoints(Integer experiencePoints) {
        this.experiencePoints = experiencePoints;
        this.level = getLevel(experiencePoints);
        this.proficiencyBonus = ((this.level - 1) / 4) + 2;
    }

    public List<Skill> getSkills() {
        return skills;
    }

    public void setSkills(List<Skill> skills) {
        this.skills = skills;
    }

    public List<Stat> getStats() {
        return stats;
    }

    public void setStats(List<Stat> stats) {
        this.stats = stats;
    }

    public List<Spell> getSpells() {
        return spells;
    }

    public void setSpells(List<Spell> spells) {
        this.spells = spells;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getGold() {
        return gold;
    }

    public void setGold(Integer gold) {
        this.gold = gold;
    }

    public Integer getSilver() {
        return silver;
    }

    public void setSilver(Integer silver) {
        this.silver = silver;
    }

    public List<SavingThrow> getSavingThrows() {
        return savingThrows;
    }

    public void setSavingThrows(List<SavingThrow> savingThrows) {
        this.savingThrows = savingThrows;
    }

    public Integer getProficiencyBonus() {
        return proficiencyBonus;
    }

    public void setProficiencyBonus(Integer proficiencyBonus) {
        this.proficiencyBonus = ((this.level - 1) / 4) + 2;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public List<SpellSlot> getSpellSlots() {
        return spellSlots;
    }

    public void setSpellSlots(List<SpellSlot> spellSlots) {
        this.spellSlots = spellSlots;
    }

    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = getLevel(this.experiencePoints);
        this.proficiencyBonus = ((this.level - 1) / 4) + 2;
    }

    private Integer getLevel(Integer experiencePoints) {
        if (experiencePoints < 300) {
            return 1;
        } else if (experiencePoints >= 300 && experiencePoints < 900) {
            return 2;
        } else if (experiencePoints >= 900 && experiencePoints < 2700) {
            return 3;
        } else if (experiencePoints >= 2700 && experiencePoints < 6500) {
            return 4;
        } else if (experiencePoints >= 6500 && experiencePoints < 14000) {
            return 5;
        } else if (experiencePoints >= 14000 && experiencePoints < 23000) {
            return 6;
        } else if (experiencePoints >= 23000 && experiencePoints < 34000) {
            return 7;
        } else if (experiencePoints >= 34000 && experiencePoints < 48000) {
            return 8;
        } else if (experiencePoints >= 48000 && experiencePoints < 64000) {
            return 9;
        } else if (experiencePoints >= 64000 && experiencePoints < 85000) {
            return 10;
        } else if (experiencePoints >= 85000 && experiencePoints < 100000) {
            return 11;
        } else if (experiencePoints >= 100000 && experiencePoints < 120000) {
            return 12;
        } else if (experiencePoints >= 120000 && experiencePoints < 140000) {
            return 13;
        } else if (experiencePoints >= 140000 && experiencePoints < 165000) {
            return 14;
        } else if (experiencePoints >= 165000 && experiencePoints < 195000) {
            return 15;
        } else if (experiencePoints >= 195000 && experiencePoints < 225000) {
            return 16;
        } else if (experiencePoints >= 225000 && experiencePoints < 265000) {
            return 17;
        } else if (experiencePoints >= 265000 && experiencePoints < 305000) {
            return 18;
        } else if (experiencePoints >= 305000 && experiencePoints < 355000) {
            return 19;
        } else {
            return 20;
        }
    }

    public Weapon getCurrentWeapon() {
        return currentWeapon;
    }

    public void setCurrentWeapon(Weapon currentWeapon) {
        this.currentWeapon = currentWeapon;
    }

    public Boolean getInspired() {
        return inspired;
    }

    public String getProficiencies() {
        return proficiencies;
    }

    public void setProficiencies(String proficiencies) {
        this.proficiencies = proficiencies;
    }

    public void setInspired(Boolean inspired) {
        this.inspired = inspired;
    }

    public List<Counter> getCounters() {
        return counters;
    }

    public void setCounters(List<Counter> counters) {
        this.counters = counters;
    }

    public List<Proficiency> getProficienciesObjs() {
        return proficienciesObjs;
    }

    public void setProficienciesObjs(List<Proficiency> proficienciesObjs) {
        this.proficienciesObjs = proficienciesObjs;
    }

    public List<Trait> getTraits() {
        return traits;
    }

    public void setTraits(List<Trait> traits) {
        this.traits = traits;
    }
}

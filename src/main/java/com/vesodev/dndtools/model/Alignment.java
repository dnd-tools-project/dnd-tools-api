package com.vesodev.dndtools.model;

/**
 * Created by vesko on 22.5.2021 г..
 */
public enum Alignment {
    LG, NG, CH, LN, TN, CN, LE, NE, CE
}

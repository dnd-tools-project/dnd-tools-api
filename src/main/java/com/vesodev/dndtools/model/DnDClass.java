package com.vesodev.dndtools.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by vesko on 25.6.2021 г..
 */
//@Entity
public class DnDClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @ManyToMany
    private List<SavingThrow> savingThrows;

    @OneToOne
    private Dice hitDice;


}

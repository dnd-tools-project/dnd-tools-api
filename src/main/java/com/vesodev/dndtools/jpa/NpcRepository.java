package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.Npc;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by vesko on 7.7.2021 г..
 */
public interface NpcRepository extends CrudRepository<Npc, Integer> {
}

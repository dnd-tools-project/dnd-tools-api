package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.SpellSlot;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by vesko on 23.5.2021 г..
 */
public interface SpellSlotRepository extends CrudRepository<SpellSlot, Integer> {
}

package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.SavingThrow;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by vesko on 24.5.2021 г..
 */
public interface SavingThrowRepository extends CrudRepository<SavingThrow, Integer> {
}

package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.Proficiency;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by vesko on 30.6.2021 г..
 */
public interface ProficiencyRepository extends CrudRepository<Proficiency, Integer> {

    public List<Proficiency> findAllByNameContains(String name);
}

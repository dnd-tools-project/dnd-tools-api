package com.vesodev.dndtools.jpa;

import com.vesodev.dndtools.model.DnDCampaign;
import com.vesodev.dndtools.model.DnDCampaignView;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by vesko on 7.7.2021 г..
 */
public interface DnDCampaignRepository extends CrudRepository<DnDCampaign, Integer> {

    @Query(value = "select id, name from dndcampaign", nativeQuery = true)
    public List<DnDCampaignView> findAllPartial();
}

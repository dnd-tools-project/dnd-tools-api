package com.vesodev.dndtools.rest;

import com.vesodev.dndtools.jpa.*;
import com.vesodev.dndtools.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by vesko on 7.7.2021 г..
 */
@RestController
public class NpcController {

    @Autowired
    private NpcRepository npcRepository;

    @Autowired
    private CharacterRepository characterRepository;

    @Autowired
    private SpellRepository spellRepository;

    @Autowired
    private StatRepository statRepository;

    @Autowired
    private SkillRepository skillRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private SavingThrowRepository savingThrowRepository;


    @RequestMapping(path = "/npcs/new", method = RequestMethod.GET)
    public Npc getNewCharacter() {
        Npc npc = new Npc();

        npc.setMaxHp(0);
        npc.setCurrentHp(0);
        npc.setArmorClass(0);
        npc.setProficiencyBonus(0);
        npc.setItems(new ArrayList<>());
        npc.setStats(new ArrayList<>());
        npc.setSkills(new ArrayList<>());
        npc.setSavingThrows(new ArrayList<>());

        for (StatType statType : StatType.values()) {
            Stat stat = new Stat();
            stat.setType(statType);
            stat.setValue(0);
            npc.getStats().add(stat);

            SavingThrow savingThrow = new SavingThrow();
            savingThrow.setHasProficiency(false);
            savingThrow.setType(statType);
            npc.getSavingThrows().add(savingThrow);
        }

        for (SkillType skillType : SkillType.values()) {
            Skill skill = new Skill();
            skill.setSkillType(skillType);
            npc.getSkills().add(skill);
        }

        return npc;
    }

    @GetMapping("/npcs")
    public List<Npc> getAll() {
        return (List<Npc>) this.npcRepository.findAll();
    }

    @GetMapping("/npc/{id}")
    public Npc getById(@PathVariable Integer id) {
        Optional<Npc> npc = npcRepository.findById(id);
        if (npc.isPresent()) {
            return npc.get();
        }
        throw new EntityNotFoundException("Npc Not Found");
    }
}

package com.vesodev.dndtools.rest;

import com.vesodev.dndtools.jpa.TraitRepository;
import com.vesodev.dndtools.model.Trait;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by vesko on 30.6.2021 г..
 */
@RestController
public class TraitController {

    @Autowired
    private TraitRepository traitRepository;

    @PostMapping(path = "/traits")
    public Trait addTrait(@RequestBody Trait trait) {
        return traitRepository.save(trait);
    }

    @GetMapping(path = "/traits")
    public List<Trait> getTraits() {
        return (List<Trait>) this.traitRepository.findAll();
    }

    @GetMapping(path = "/traits/{traitName}")
    public List<Trait> findByName(@PathVariable("traitName") String traitName) {
        return this.traitRepository.findAllByNameContains(traitName);
    }
}

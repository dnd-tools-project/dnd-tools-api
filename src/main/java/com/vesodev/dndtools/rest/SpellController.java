package com.vesodev.dndtools.rest;

import com.vesodev.dndtools.jpa.SpellRepository;
import com.vesodev.dndtools.model.Spell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

/**
 * Created by vesko on 2.6.2021 г..
 */
@RestController
public class SpellController {

    @Autowired
    private SpellRepository spellRepository;

    @RequestMapping(path = "/spells", method = RequestMethod.GET)
    public List<Spell> getSpells() {
        return this.spellRepository.getAll();
    }

    @GetMapping(path = "/spells/{spellId}")
    public Spell getSpell(@PathVariable Integer spellId) {
        Optional<Spell> spell = this.spellRepository.findById(spellId);
        if (!spell.isPresent()) {
            throw new EntityNotFoundException("Spell not found");
        }
        return spell.get();
    }

    @PostMapping(path = "/spells")
    public Spell addSpell(@RequestBody Spell spell) {
        return this.spellRepository.save(spell);
    }

    @PostMapping(path = "/add-spells")
    public Iterable<Spell> addSpells(@RequestBody List<Spell> spells) {

        return this.spellRepository.saveAll(spells);
    }

    @GetMapping(path = "/spells/{classname}/{maxLvl}")
    public List<Spell> getSpellsByClassname(@PathVariable String classname, @PathVariable Integer maxLvl) {
        return this.spellRepository.findByClassesContainsAndSpellSlotLessThanEqual(classname, maxLvl);
    }

    @GetMapping(path = "/spells/namecontains/{spellName}")
    public List<Spell> getSpellByName(@PathVariable String spellName) {
        return this.spellRepository.findAllByNameContains(spellName);
    }

    @DeleteMapping(path = "/spells/{spellId}")
    public void deleteSpell(@PathVariable Integer spellId) {
        this.spellRepository.deleteById(spellId);
    }

}

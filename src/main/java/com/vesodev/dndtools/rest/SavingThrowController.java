package com.vesodev.dndtools.rest;

import com.vesodev.dndtools.jpa.SavingThrowRepository;
import com.vesodev.dndtools.model.SavingThrow;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vesko on 24.6.2021 г..
 */
@RestController
public class SavingThrowController {
    @Autowired
    private SavingThrowRepository savingThrowRepository;

    @PutMapping(path = "/throws")
    public SavingThrow updateSkill(@RequestBody SavingThrow savingThrow) {
        return this.savingThrowRepository.save(savingThrow);
    }
}

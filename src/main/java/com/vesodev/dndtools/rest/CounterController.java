package com.vesodev.dndtools.rest;

import com.vesodev.dndtools.jpa.CounterRepository;
import com.vesodev.dndtools.model.Counter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vesko on 24.6.2021 г..
 */
@RestController
public class CounterController {

    @Autowired
    private CounterRepository counterRepository;

    @PutMapping(path = "/counters")
    public Counter updateCounter(@RequestBody Counter counter) {
        return this.counterRepository.save(counter);
    }
}

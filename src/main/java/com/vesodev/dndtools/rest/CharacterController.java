package com.vesodev.dndtools.rest;

import com.vesodev.dndtools.jpa.*;
import com.vesodev.dndtools.model.Character;
import com.vesodev.dndtools.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by vesko on 22.5.2021 г..
 */
@RestController
public class CharacterController {

    private final CharacterRepository characterRepository;

    private final SpellRepository spellRepository;

    private final StatRepository statRepository;

    private final SkillRepository skillRepository;

    private final DiceRepository diceRepository;

    private final ItemRepository itemRepository;

    private final SpellSlotRepository spellSlotRepository;

    private final SavingThrowRepository savingThrowRepository;

    private final CounterRepository counterRepository;

    public CharacterController(CharacterRepository characterRepository, SpellRepository spellRepository, StatRepository statRepository, SkillRepository skillRepository, DiceRepository diceRepository, ItemRepository itemRepository, SpellSlotRepository spellSlotRepository, SavingThrowRepository savingThrowRepository, CounterRepository counterRepository) {
        this.characterRepository = characterRepository;
        this.spellRepository = spellRepository;
        this.statRepository = statRepository;
        this.skillRepository = skillRepository;
        this.diceRepository = diceRepository;
        this.itemRepository = itemRepository;
        this.spellSlotRepository = spellSlotRepository;
        this.savingThrowRepository = savingThrowRepository;
        this.counterRepository = counterRepository;
    }

    @RequestMapping(path = "/characters", method = RequestMethod.GET)
    public List<Character> getCharacters() {
        return (List<Character>) characterRepository.findAll();
    }

    @RequestMapping(path = "/characters", method = RequestMethod.POST)
    public Character postCharacters(@RequestBody Character character) {
        if (character.getSpells() != null) {
            spellRepository.saveAll(character.getSpells());
        }
        if (character.getSkills() != null) {
            skillRepository.saveAll(character.getSkills());
        }
        if (character.getStats() != null) {
            statRepository.saveAll(character.getStats());
        }
        if (character.getHitDice() != null) {
            diceRepository.save(character.getHitDice());
        }
        if (character.getItems() != null) {
            for (Item item : character.getItems()) {
                if (item.getClass().equals(Weapon.class)) {
                    this.diceRepository.save(((Weapon) item).getDamage());
                }
            }
            this.itemRepository.saveAll(character.getItems());
        }
        if (character.getSpellSlots() != null) {
            spellSlotRepository.saveAll(character.getSpellSlots());
        }
        if (character.getCounters() != null) {
            counterRepository.saveAll(character.getCounters());
        }
        if (character.getSavingThrows() != null) {
            savingThrowRepository.saveAll(character.getSavingThrows());
        }
        characterRepository.save(character);
        return characterRepository.findByName(character.getName());
    }


    @RequestMapping(path = "/characters", method = RequestMethod.PUT)
    public Character putCharacters(@RequestBody Character character) {
        Optional<Character> fullChar = this.characterRepository.findById(character.getId());
        if (fullChar.isPresent()) {
            fullChar.get().setArmorClass(character.getArmorClass());
            fullChar.get().setGold(character.getGold());
            fullChar.get().setSilver(character.getSilver());
            fullChar.get().setMaxHp(character.getMaxHp());
            fullChar.get().setCurrentHp(character.getCurrentHp());
            fullChar.get().setExperiencePoints(character.getExperiencePoints());
            fullChar.get().setNotes(character.getNotes());
            fullChar.get().setProficiencies(character.getProficiencies());
            fullChar.get().setInspired(character.getInspired());
            fullChar.get().setSpeed(character.getSpeed());
            fullChar.get().setAlignment(character.getAlignment());
            character.setLevel(fullChar.get().getLevel());
            characterRepository.save(fullChar.get());
            return character;
        }
        throw new EntityNotFoundException("Character Not found");
    }

    @RequestMapping(path = "/characters/{name}", method = RequestMethod.GET)
    public Character getCharacterByName(@PathVariable String name) {
        return characterRepository.findByName(name);
    }

    @RequestMapping(path = "/characters/new", method = RequestMethod.GET)
    public Character getNewCharacter() {
        Character character = new Character();

        character.setMaxHp(0);
        character.setCurrentHp(0);
        character.setArmorClass(0);
        character.setSpeed(0);
        character.setExperiencePoints(0);
        character.setGold(0);
        character.setProficiencyBonus(0);
        character.setSilver(0);
        character.setLevel(1);
        character.setItems(new ArrayList<>());
        character.setStats(new ArrayList<>());
        character.setSkills(new ArrayList<>());
        character.setSavingThrows(new ArrayList<>());

        for (StatType statType : StatType.values()) {
            Stat stat = new Stat();
            stat.setType(statType);
            stat.setValue(0);
            character.getStats().add(stat);

            SavingThrow savingThrow = new SavingThrow();
            savingThrow.setHasProficiency(false);
            savingThrow.setType(statType);
            character.getSavingThrows().add(savingThrow);
        }

        for (SkillType skillType : SkillType.values()) {
            Skill skill = new Skill();
            skill.setSkillType(skillType);
            character.getSkills().add(skill);
        }

        return character;
    }

    @RequestMapping(path = "/players/{name}", method = RequestMethod.GET)
    public List<CharacterView> getCharactersByPlayerName(@PathVariable String name) {
        return characterRepository.findAllByPlayerName(name);
    }

    @RequestMapping(path = "/characters/{charId}/items", method = RequestMethod.POST)
    public Item addItem(@PathVariable Integer charId, @RequestBody Item item) {
        Optional<Character> character = characterRepository.findById(charId);
        if (character.isPresent()) {
            if (item.getClass().equals(Weapon.class)) {
                diceRepository.save(((Weapon) item).getDamage());
            }
            Item savedItem = itemRepository.save(item);
            character.get().getItems().add(savedItem);
            characterRepository.save(character.get());
            return savedItem;
        }
        throw new EntityNotFoundException("Character Not found");
    }

    @RequestMapping(path = "/characters/{charId}/items/{itemId}", method = RequestMethod.DELETE)
    public void deleteItem(@PathVariable Integer charId, @PathVariable Integer itemId) {
        Optional<Character> character = characterRepository.findById(charId);
        if (character.isPresent()) {
            if (character.get().getCurrentWeapon() != null) {
                if (character.get().getCurrentWeapon().getId().equals(itemId)) {
                    character.get().setCurrentWeapon(null);
                }
            }
            Item itemToRemove = null;
            for (Item item : character.get().getItems()) {
                if (item.getId().equals(itemId)) {
                    itemToRemove = item;
                    break;
                }
            }
            if (itemToRemove != null) {
                character.get().getItems().remove(itemToRemove);
            }
            characterRepository.save(character.get());
        }

        itemRepository.deleteById(itemId);
    }

    @RequestMapping(path = "/characters/{charId}/spells", method = RequestMethod.PUT)
    public Spell updateSpell(@PathVariable Integer charId, @RequestBody Spell spell) {
        Optional<Character> character = this.characterRepository.findById(charId);
        if (!character.isPresent()) {
            throw new EntityNotFoundException("Character Not Found");
        }
        character.get().getSpells().add(spell);
        this.characterRepository.save(character.get());
        return spell;
    }

    @RequestMapping(path = "/characters/{charId}/spells/{spellId}", method = RequestMethod.DELETE)
    public void deleteSpell(@PathVariable Integer charId, @PathVariable Integer spellId) {
        Optional<Character> character = characterRepository.findById(charId);
        if (character.isPresent()) {
            Spell spellToRemove = null;
            for (Spell spell : character.get().getSpells()) {
                if (spell.getId().equals(spellId)) {
                    spellToRemove = spell;
                    break;
                }
            }
            if (spellToRemove != null) {
                character.get().getSpells().remove(spellToRemove);
            }
            characterRepository.save(character.get());
        }
    }

    @RequestMapping(path = "/characters/{charId}/traits", method = RequestMethod.PUT)
    public Trait updateTrait(@PathVariable Integer charId, @RequestBody Trait trait) {
        Optional<Character> character = this.characterRepository.findById(charId);
        if (!character.isPresent()) {
            throw new EntityNotFoundException("Character Not Found");
        }
        character.get().getTraits().add(trait);
        this.characterRepository.save(character.get());
        return trait;
    }

    @RequestMapping(path = "/characters/{charId}/traits/{traitId}", method = RequestMethod.DELETE)
    public void deleteTrait(@PathVariable Integer charId, @PathVariable Integer traitId) {
        Optional<Character> character = characterRepository.findById(charId);
        if (character.isPresent()) {
            Trait traitToRemove = null;
            for (Trait trait : character.get().getTraits()) {
                if (trait.getId().equals(traitId)) {
                    traitToRemove = trait;
                    break;
                }
            }
            if (traitToRemove != null) {
                character.get().getTraits().remove(traitToRemove);
            }
            characterRepository.save(character.get());
        }
    }

    @RequestMapping(path = "/characters/{charId}/profs", method = RequestMethod.PUT)
    public Proficiency updateProf(@PathVariable Integer charId, @RequestBody Proficiency proficiency) {
        Optional<Character> character = this.characterRepository.findById(charId);
        if (!character.isPresent()) {
            throw new EntityNotFoundException("Character Not Found");
        }
        character.get().getProficienciesObjs().add(proficiency);
        this.characterRepository.save(character.get());
        return proficiency;
    }

    @RequestMapping(path = "/characters/{charId}/profs/{profId}", method = RequestMethod.DELETE)
    public void deleteProf(@PathVariable Integer charId, @PathVariable Integer profId) {
        Optional<Character> character = characterRepository.findById(charId);
        if (character.isPresent()) {
            Proficiency proficiencyToRemove = null;
            for (Proficiency proficiency : character.get().getProficienciesObjs()) {
                if (proficiency.getId().equals(profId)) {
                    proficiencyToRemove = proficiency;
                    break;
                }
            }
            if (proficiencyToRemove != null) {
                character.get().getProficienciesObjs().remove(proficiencyToRemove);
            }
            characterRepository.save(character.get());
        }
    }

    @RequestMapping(path = "/characters/{charId}/counters", method = RequestMethod.POST)
    public Counter createCounter(@PathVariable Integer charId, @RequestBody Counter counter) {
        Optional<Character> character = characterRepository.findById(charId);
        if (character.isPresent()) {
            Counter savedCounter = this.counterRepository.save(counter);
            character.get().getCounters().add(savedCounter);
            this.characterRepository.save(character.get());
            return savedCounter;
        }
        throw new EntityNotFoundException("Character Not Found");
    }

    @RequestMapping(path = "/characters/{charId}/slots", method = RequestMethod.POST)
    public SpellSlot createSlot(@PathVariable Integer charId, @RequestBody SpellSlot spellSlot) {
        Optional<Character> character = characterRepository.findById(charId);
        if (character.isPresent()) {
            SpellSlot savedSpellSlot = this.spellSlotRepository.save(spellSlot);
            character.get().getSpellSlots().add(savedSpellSlot);
            this.characterRepository.save(character.get());
            return savedSpellSlot;
        }
        throw new EntityNotFoundException("Character Not Found");
    }

    @RequestMapping(path = "/characters/{charId}/slots/{slotId}", method = RequestMethod.DELETE)
    public void deleteSlot(@PathVariable Integer charId, @PathVariable Integer slotId) {
        Optional<Character> character = characterRepository.findById(charId);
        if (character.isPresent()) {
            SpellSlot spellSlotToRemove = null;
            for (SpellSlot slot : character.get().getSpellSlots()) {
                if (slot.getId().equals(slotId)) {
                    spellSlotToRemove = slot;
                    break;
                }
            }
            if (spellSlotToRemove != null) {
                character.get().getSpellSlots().remove(spellSlotToRemove);
            }
            characterRepository.save(character.get());
        }
        spellSlotRepository.deleteById(slotId);
    }

    @RequestMapping(path = "/characters/{charId}/counters/{counterId}", method = RequestMethod.DELETE)
    public void deleteCounter(@PathVariable Integer charId, @PathVariable Integer counterId) {
        Optional<Character> character = characterRepository.findById(charId);
        if (character.isPresent()) {
            Counter counterToRemove = null;
            for (Counter counter : character.get().getCounters()) {
                if (counter.getId().equals(counterId)) {
                    counterToRemove = counter;
                    break;
                }
            }
            if (counterToRemove != null) {
                character.get().getCounters().remove(counterToRemove);
            }
            characterRepository.save(character.get());
        }
        counterRepository.deleteById(counterId);
    }

    @RequestMapping(path = "/characters/delete", method = RequestMethod.POST)
    public void deleteCharacter(@RequestBody Character character) {
        characterRepository.delete(character);

        if (character.getSpells() != null) {
            spellRepository.deleteAll(character.getSpells());
        }
        if (character.getSkills() != null) {
            skillRepository.deleteAll(character.getSkills());
        }
        if (character.getStats() != null) {
            statRepository.deleteAll(character.getStats());
        }
        if (character.getHitDice() != null) {
            diceRepository.delete(character.getHitDice());
        }
        if (character.getItems() != null) {
            this.itemRepository.deleteAll(character.getItems());
        }
        if (character.getSpellSlots() != null) {
            spellSlotRepository.deleteAll(character.getSpellSlots());
        }
        if (character.getSavingThrows() != null) {
            savingThrowRepository.deleteAll(character.getSavingThrows());
        }
    }

    @RequestMapping(path = "/characters/{charId}/weapon", method = RequestMethod.PUT)
    public Weapon setWeapon(@PathVariable Integer charId, @RequestBody Weapon weapon) {
        Optional<Character> character = characterRepository.findById(charId);
        if (character.isPresent()) {
            character.get().setCurrentWeapon(weapon);
            this.characterRepository.save(character.get());
            return weapon;
        }
        throw new EntityNotFoundException("Character Not Found");
    }

}
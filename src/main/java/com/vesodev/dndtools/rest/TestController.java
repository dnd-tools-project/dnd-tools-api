package com.vesodev.dndtools.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping(path = "/api/user")
public class TestController {

    @GetMapping("")
    public Principal getPrincipal(Principal principal) {
        return principal;
    }
}
